/**
* @author Sharifullin Nail
* 11-507
* 007
*/
class Task007 {
public static void main (String args []) {
int a=10;
int b=5;
System.out.println("a + b = " + (a + b));
System.out.println("a - b = " + (a-b));
System.out.println("b - a = " + (b-a));
System.out.println("a * b = " + (a*b));
System.out.println("a % b = " + (a%b));
System.out.println("b / a = " + (b/a));
System.out.println("b % a = " + (b%a));
}
}